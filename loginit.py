from tkinter import*
from ttk import Button,Entry

class Main():
    def __init__(self,parent):
        self.parent=parent
        self.parent.title("Login")

        self.page=StringVar()
        self.loginName=StringVar()
        self.loginPass=StringVar()
        self.signinName=StringVar()
        self.signinPass=StringVar()
        self.sts=StringVar()

        self.createWidgets()
        self.showlogin()

    def createWidgets(self):
        Label(self.parent,textvariable=self.page,font=("",20)).pack()
        frame1=Frame(self.parent)
        Label(frame1,text="Name").grid(sticky=W)
        Entry(frame1,textvariable=self.loginName).grid(row=0,column=1, pady=10,padx=10)
        Label(frame1,text="Password").grid(sticky=W)
        Entry(frame1,textvariable=self.loginPass, show="*").grid(row=1,column=1)
        Button(frame1,text="Login", command=self.login).grid(pady=10)
        Button(frame1, text="Sign in", command=self.signin).grid(row=2,column=1,pady=10)
        frame1.pack(padx=10, pady=10)
        frame2=Frame(self.parent)

        Label(frame2, text="Name").grid(sticky=W)
        Entry(frame2, textvariable=self.signinName).grid(row=0, column=1, pady=10, padx=10)
        Label(frame2, text="Password").grid(sticky=W)
        Entry(frame2, textvariable=self.signinPass, show="*").grid(row=1, column=1)
        Button(frame2,text="Create", command=self.create).grid(pady=10)
        Button(frame2, text="Back", command=self.showlogin).grid(row=2,column=1,pady=10)
        frame3=Frame(self.parent)
        Label(frame3,text="Loged in",font=("",50)).pack(padx=10,pady=10)
        self.loginFrame=frame1
        self.signinFrame=frame2
        self.logedFrame=frame3
        Label(self.parent,textvariable=self.sts).pack()


    def login(self):
        name=self.loginName.get()
        password=self.loginPass.get()
        try:
            f=open("savedata", "r")
            cname=f.read()
            f1=open("savepass", "r")
            cpass=f.read()
            if name==cname and password==cpass:
                self.showloged()
            else:
                self.sts.set("Wrong Name")
        except:
            self.sts.set("Wrong Name and Password")

    def signin(self):
        self.page.set("Sign in")
        self.loginFrame.pack_forget()
        self.signinFrame.pack()

    def showlogin(self):
        self.page.set("Login")
        self.signinFrame.pack_forget()
        self.loginFrame.pack()

    def showloged(self):
        self.loginFrame.pack_forget()
        self.logedFrame.pack()


    def create(self):
        name=self.signinName.get()
        password=self.signinPass.get()
        f=open("savename","w")
        f.write(format(name))
        f=open("savepass","w")
        f.write(format(password))
        f.close()
        self.showlogin()

if __name__=="__main__":
    root=Tk()
    Main(root)
    root.mainloop()
