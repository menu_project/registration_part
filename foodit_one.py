from tkinter import *
from tkinter import messagebox
from tkinter import ttk
import random
import time;
import datetime
from tkinter import Tk, StringVar, ttk





root=Tk()
root.geometry("1350x750+0+0")
root.title("Foodit :)")
root.configure(background="lightblue")
var0=None
our=[]
golo=0
food=0
totalit=0
currentDT=0
allmoney=0



Tops=Frame(root,width=600,height=100, bd=12, relief="raise")
Tops.pack(side=TOP, fill=BOTH)

lblTitle=Label(Tops,font=("arial", 48, "bold"), text="                    Welcome to Foodit")
lblTitle.grid(row=0, column=0)

BottomMainFrame=Frame(root, borderwidth = 4, bd = 4, relief="raise")
BottomMainFrame.pack(side=BOTTOM)

f1=Frame(BottomMainFrame, borderwidth = 4, bd = 4, relief="raise")
f1.pack(side=LEFT, expand = YES, fill = BOTH)
f2=Frame(BottomMainFrame, borderwidth = 4, bd = 4, relief="raise")
f2.pack(side=RIGHT, expand = YES, fill = BOTH)

f2TOP=Frame(f2, borderwidth = 4, bd = 4, relief="raise")
f2TOP.pack(side=TOP, expand = YES, fill = BOTH)
# f2TOP.configure(background="blue")
f2BOTTOM=Frame(f2, borderwidth = 4, bd = 4, relief="raise")
f2BOTTOM.pack(side=BOTTOM)

var0=IntVar()
var1=IntVar()
var2=IntVar()
var3=IntVar()
var4=IntVar()
var5=IntVar()
var6=IntVar()
var7=IntVar()
var8=IntVar()
var9=IntVar()
var10=IntVar()
var11=IntVar()
var12=IntVar()
var13=IntVar()
var14=IntVar()
var15=IntVar()
var16=IntVar()
var17=IntVar()
var18=IntVar()

var1.set(0)
var1.set(0)
var2.set(0)
var3.set(0)
var4.set(0)
var5.set(0)
var6.set(0)
var7.set(0)
var8.set(0)
var9.set(0)
var10.set(0)
var11.set(0)
var12.set(0)
var13.set(0)
var14.set(0)
var15.set(0)
var16.set(0)
var17.set(0)
var18.set(0)

varTaco=StringVar()
varBurrito=StringVar()
varKabab=StringVar()
varVegPizza=StringVar()
varOlive=StringVar()
varSunSalad=StringVar()
varSalad=StringVar()
varFries=StringVar()
varTea=StringVar()
varCoffee=StringVar()
varJuice=StringVar()
varChange=StringVar()
varSubTotal=StringVar()
varTotal=StringVar()
varVat=StringVar()
varTax=StringVar()
varTable=StringVar()


varTaco.set("")
varBurrito.set("")
varKabab.set("")
varVegPizza.set("")
varOlive.set("")
varSunSalad.set("")
varSalad.set("")
varFries.set("")
varTea.set("")
varCoffee.set("")
varJuice.set("")
varChange.set("")
varSubTotal.set("")
varTotal.set("")
varVat.set("")
varTax.set("")
varTable.set("")

############################################### EXIT button function ###################################################
def iExit():
    qExit = messagebox.askyesno("Quit System", "Do you want to quit?")
    if qExit > 0:
        root.destroy()
        return


############################################## Frame 2 Bottom Exit to exit #############################################

def iReset():

    varTaco.set("")
    varBurrito.set("")
    varKabab.set("")
    varVegPizza.set("")
    varOlive.set("")
    varSunSalad.set("")
    varSalad.set("")
    varFries.set("")
    varTea.set("")
    varCoffee.set("")
    varJuice.set("")
    varChange.set("")
    varSubTotal.set("")
    varTotal.set("")
    varVat.set("")
    varTax.set("")
    varTable.set("")
    our=[]
    textBox.delete('1.0', END)
    totalit=0


    txtTaco.configure(state=NORMAL)
    txtBurrito.configure(state=NORMAL)
    txtKabab.configure(state=NORMAL)
    txtPizza.configure(state=NORMAL)
    txtOlive.configure(state=NORMAL)
    txtSunSalad.configure(state=NORMAL)
    txtSalad.configure(state=NORMAL)
    txtFries.configure(state=NORMAL)
    txtTea.configure(state=NORMAL)
    txtCoffee.configure(state=NORMAL)
    txtJuice.configure(state=NORMAL)
    # txtTax.configure(state=NORMAL)
    #txtPayment.configure(state=NORMAL)
    #txtSubTotal.configure(state=NORMAL)
    #txtTolal.configure(state=NORMAL)
    txtTable.configure(state=NORMAL)

############################################# Total button ############################################################


def ilist(food, full):
    our.append(food)
    our.append(full)

    print("inside here ", our)

def iTable():  # event function for ptest
    global golo
    global food
    golo = int(txtTable.get())
    food="\t\tTable: "
    addText(food, golo)


def iTaco():
    global golo
    global food
    global totalit
    golo = int(txtTaco.get())
    totalit += golo * 7000

    food="Taco "
    ilist(food, golo)
    addText(food, golo)
def iBuritto():
    global golo
    global food
    global totalit
    golo = int(txtBurrito.get())
    totalit+=golo*7000

    food="Burrito "
    ilist(food, golo)
    addText(food, golo)
def iKabab():
    global golo
    global food
    global totalit
    golo = int(txtKabab.get())
    totalit += golo * 6000

    food="Kabab "
    ilist(food, golo)
    addText(food, golo)
def iVegpizza():
    global golo
    global food
    global totalit
    golo = int(txtPizza.get())
    totalit+=golo*8000
    food = "Vegpizza "
    ilist(food, golo)
    addText(food, golo)
def iOlive():
    global golo
    global food
    global totalit
    golo = int(txtOlive.get())
    totalit +=golo*6000

    food = "Olive "
    ilist(food, golo)
    addText(food, golo)
def iSunSalad():
    global golo
    global food
    global totalit
    golo = int(txtSunSalad.get())
    totalit+=golo*6500

    food = "SunSalad "
    ilist(food, golo)
    addText(food, golo)
def iSalad():
    global golo
    global food
    global totalit
    golo = int(txtSalad.get())
    totalit+=golo*5000

    food = "Salad "
    ilist(food, golo)
    addText(food, golo)
def iFries():
    global golo
    global food
    global totalit
    golo = int(txtFries.get())
    totalit+=golo*3500

    food = "Fries "
    ilist(food, golo)
    addText(food, golo)
def iTea():
    global golo
    global food
    global totalit
    golo = int(txtTea.get())
    totalit+=golo*2000

    food = "Tea "
    ilist(food, golo)
    addText(food, golo)
def iCoffee():
    global golo
    global food
    global totalit
    golo = int(txtCoffee.get())
    totalit+=golo*2000

    food = "Coffee "
    ilist(food, golo)
    addText(food, golo)
def iJuice():
    global golo
    global food
    global totalit
    golo = int(txtJuice.get())
    totalit+=golo*1000

    food = "Juice "
    ilist(food, golo)
    addText(food, golo)

def Showtotal():
    global totalit
    global currentDT
    global our
    global allmoney
    if totalit > 0:
        addText("\t\tTotal: ", totalit)
        currentDT = datetime.datetime.now()
        addText("\t\tTime: ", currentDT)
        fw=open("Order.txt", 'w')
        fw.write(str(our))
        fw.close()
        allmoney+=totalit
    else:
        pass
################################################ Food section ##########################################################
lblMeal=Label(f1, font=("arial", 18,"bold"), text="\t<<< Food >>>")
lblMeal.grid(row=0,column=0)

Taco=Label(f1, text="Taco\t\t\t₩ 7.000",font=('arial', 18, 'bold') ).grid(row=1, column=0)
Tacobtn = Button(f1, text="OK", width=10, command=iTaco).grid(row=1, column=2)
txtTaco=Entry(f1, font=('arial', 18, 'bold'), textvariable=varTaco, width=10, justify="right", state=NORMAL)
txtTaco.grid(row=1,column=1)

Burrito=Label(f1, text="Burrito\t\t\t₩ 7.000",font=('arial', 18, 'bold') ).grid(row=2, column=0)
Burritobtn = Button(f1, text="OK", width=10, command=iBuritto).grid(row=2, column=2)
txtBurrito=Entry(f1, font=('arial', 18, 'bold'), textvariable=varBurrito, width=10, justify="right", state=NORMAL)
txtBurrito.grid(row=2,column=1)

Kabab=Label(f1, text="Kabab\t\t\t₩ 6.000",font=('arial', 18, 'bold') ).grid(row=3, column=0)
Kababbtn = Button(f1, text="OK", width=10, command=iKabab).grid(row=3, column=2)
txtKabab=Entry(f1, font=('arial', 18, 'bold'), textvariable=varKabab, width=10, justify="right", state=NORMAL)
txtKabab.grid(row=3,column=1)

Vegpizza=Label(f1, text="Vegpizza\t\t\t₩ 8.000",font=('arial', 18, 'bold') ).grid(row=4, column=0)
Vegpizzabtn = Button(f1, text="OK", width=10, command=iVegpizza).grid(row=4, column=2)
txtPizza=Entry(f1, font=('arial', 18, 'bold'), textvariable=varVegPizza, width=10, justify="right", state=NORMAL)
txtPizza.grid(row=4,column=1)

##################################################### Salads ###########################################################
lblMeal=Label(f1, font=("arial", 18,"bold"), text="\t<<< Salads >>>")
lblMeal.grid(row=5,column=0)

Olive=Label(f1, text="Olive\t\t\t₩ 6.000",font=('arial', 18, 'bold') ).grid(row=6, column=0)
Olivebtn = Button(f1, text="OK", width=10, command=iOlive).grid(row=6, column=2)
txtOlive=Entry(f1, font=('arial', 18, 'bold'), textvariable=varOlive, width=10, justify="right", state=NORMAL)
txtOlive.grid(row=6,column=1)

SunSalad=Label(f1, text="SunSalad\t\t₩ 6.500",font=('arial', 18, 'bold') ).grid(row=7, column=0)
SunSaladbtn = Button(f1, text="OK", width=10, command=iSunSalad).grid(row=7, column=2)
txtSunSalad=Entry(f1, font=('arial', 18, 'bold'), textvariable=varSunSalad, width=10, justify="right", state=NORMAL)
txtSunSalad.grid(row=7,column=1)

Salad=Label(f1, text="Salad\t\t\t₩ 5.000",font=('arial', 18, 'bold') ).grid(row=8, column=0)
Saladbtn = Button(f1, text="OK", width=10, command=iSalad).grid(row=8, column=2)
txtSalad=Entry(f1, font=('arial', 18, 'bold'), textvariable=varSalad, width=10, justify="right", state=NORMAL)
txtSalad.grid(row=8,column=1)

Fries=Label(f1, text="Fries\t\t\t₩ 3.500",font=('arial', 18, 'bold') ).grid(row=9, column=0)
Friesbtn = Button(f1, text="OK", width=10, command=iFries).grid(row=9, column=2)
txtFries=Entry(f1, font=('arial', 18, 'bold'), textvariable=varFries, width=10, justify="right", state=NORMAL)
txtFries.grid(row=9,column=1)

##################################################### Bevarages ########################################################
lblMeal=Label(f1, font=("arial", 18,"bold"), text="\t<<< Beverages >>>")
lblMeal.grid(row=10,column=0)

Tea=Label(f1, text="Tea\t\t\t₩ 2.000",font=('arial', 18, 'bold') ).grid(row=11, column=0)
Teabtn = Button(f1, text="OK", width=10, command=iTea).grid(row=11, column=2)
txtTea=Entry(f1, font=('arial', 18, 'bold'), textvariable=varTea, width=10, justify="right", state=NORMAL)
txtTea.grid(row=11,column=1)

Coffee=Label(f1, text="Coffee\t\t\t₩ 2.000",font=('arial', 18, 'bold') ).grid(row=12, column=0)
Coffeebtn = Button(f1, text="OK", width=10, command=iCoffee).grid(row=12, column=2)
txtCoffee=Entry(f1, font=('arial', 18, 'bold'), textvariable=varCoffee, width=10, justify="right", state=NORMAL)
txtCoffee.grid(row=12,column=1)

Juice=Label(f1, text="Juice\t\t\t₩ 1.000",font=('arial', 18, 'bold') ).grid(row=13, column=0)
Juicebtn = Button(f1, text="OK", width=10, command=iJuice).grid(row=13, column=2)
txtJuice=Entry(f1, font=('arial', 18, 'bold'), textvariable=varJuice, width=10, justify="right", state=NORMAL)
txtJuice.grid(row=13,column=1)

lblMeal=Label(f1, font=("arial", 18,"bold"), text="")
lblMeal.grid(row=15,column=0)

lblMeal=Label(f1, font=("arial", 18,"bold"), text="")
lblMeal.grid(row=16,column=0)

lblMeal=Label(f1, font=("arial", 18,"bold"), text="")
lblMeal.grid(row=17,column=0)

lblMeal=Label(f1, font=("arial", 18,"bold"), text="")
lblMeal.grid(row=18,column=0)

lblMeal=Label(f1, font=("arial", 18,"bold"), text="")
lblMeal.grid(row=19,column=0)

lblMeal=Label(f1, font=("arial", 18,"bold"), text="")
lblMeal.grid(row=20,column=0)

lblMeal=Label(f1, font=("arial", 18,"bold"), text="")
lblMeal.grid(row=21,column=0)


############################################# Frame 2 BOTTOM ###########################################################
lblPaymentMethod=Label(f2BOTTOM, font=('arial', 14, 'bold'), text="              Payment Method",
                       bd=10, width=25, anchor='w')
lblPaymentMethod.grid(row=0,column=0)

lblChange=Label(f2BOTTOM, font=('arial', 14, 'bold'), text="", bd=10, anchor='w')
lblChange.grid(row=0, column=1)
# txtChange=Entry(f2BOTTOM, font=('arial', 18, 'bold'), textvariable=varChange, width=10, state=NORMAL)
# txtChange.grid(row=0, column=2)

cmbPaymentMethod=ttk.Combobox(f2BOTTOM, textvariable=var13, state="readonly", font=("arial", 10, "bold"),width=20)
cmbPaymentMethod["value"]=('Cash', 'Master Card', 'Visa Card', 'Debit Card')
cmbPaymentMethod.current(0)
cmbPaymentMethod.grid(row=1, column=0)

lblTax=Label(f2BOTTOM, font=("arial", 14, "bold"), text="", bd=10, anchor='w')
lblTax.grid(row=1, column=1)
# txtTax=Entry(f2BOTTOM, font=("arial", 18, "bold"), textvariable=varSubTotal, width=10, state=NORMAL )
# txtTax.grid(row=1, column=2)

 # txtPayment=Entry(f2BOTTOM, font=("arial", 18, "bold"), textvariable=varChange, width=10, state=NORMAL)
 # txtPayment.grid(row=2, column=2)
lblSubTotal=Label(f2BOTTOM, font=("arial", 14, "bold"), text="", bd=10, anchor='w')
lblSubTotal.grid(row=2, column=1)
 # txtSubTotal=Entry(f2BOTTOM, font=("arial", 18, "bold"), textvariable=varSubTotal, width=20, state=NORMAL)

lableTotal=Label(f2BOTTOM, font=("arial", 14, "bold"), text="", bd=10, width=6, anchor='w')
lableTotal.grid(row=2, column=1)
# txtTolal=Entry(f2BOTTOM, font=("arial", 18, "bold"), textvariable=varTotal, width=10, state=NORMAL)
# txtTolal.grid(row=2, column=2)

lblPayment=Label(f2BOTTOM, font=("arial", 14, "bold"), text="", bd=10, width=6, anchor='w')
lblPayment.grid(row=3, column=1)
# txtPayment=Entry(f2BOTTOM, font=("arial", 18, "bold"), textvariable=varTotal, width=10, state=NORMAL)
# txtPayment.grid(row=3, column=2)

######################################### Frame2 Bottom Button #########################################################

btnTotal=Button(f2BOTTOM, padx=16, pady=1, bd=4, fg="black", font=("arial", 16, "bold"), width=5, text="Total", command=Showtotal).grid(row=3, column=0)

btnReset=Button(f2BOTTOM, padx=16, pady=1, bd=4, fg="black", font=("arial", 16, "bold"), width=5, text="Pay", command= iReset).grid(row=3, column=1)

btnExit=Button(f2BOTTOM, padx=16, pady=1, bd=4, fg="black", font=("arial", 16, "bold"), width=5, text="Exit", command= iExit).grid(row=3, column=2)

lblMeal=Label(f2BOTTOM, font=("arial", 18,"bold"), text="")
lblMeal.grid(row=5,column=2)
lblMeal=Label(f2BOTTOM, font=("arial", 18,"bold"), text="")
lblMeal.grid(row=6,column=2)
lblMeal=Label(f2BOTTOM, font=("arial", 18,"bold"), text="")
lblMeal.grid(row=7,column=2)


lblspace=Label(f2BOTTOM, text="")
lblspace.grid(row=5, column=0)

####################################### Frame 2 Top ####################################################################
TableLB = Label(f2TOP, text="\t\t\tTable :",font=('arial', 18, 'bold')).grid(row=1, column=0)
Tablebtn = Button(f2TOP, text="OK", width=5, command=iTable).grid(row=1, column=2)
txtTable=Entry(f2TOP, font=('arial', 18, 'bold'), textvariable=varTable, width=10, justify="right", state=NORMAL)
txtTable.grid(row=1,column=1)

def showTextBox():
    textBox = Text(f2TOP, heigh=15, width=50)
    textBox.insert(END, "")
    textBox.grid(row=3, column=0)
    return textBox


def addText(food, number):
    textBox.insert(END, str(food))
    textBox.insert(END, str(number)+"\n")

textBox = showTextBox()
root.mainloop()
